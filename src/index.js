const path = require("path"); // paths on different os

require("dotenv").config(); // environment variables
const config = require(path.resolve(__dirname, "./../config"));

const debug = require("debug")("space-invaders-online-ws:src:index");
const logger = require(path.resolve(__dirname, "./utils/logger"));

console.log(`config: ${JSON.stringify(config)}`);
