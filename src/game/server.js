const path = require("path"); // paths on different os

require("dotenv").config(); // environment variables
const config = require(path.resolve(__dirname, "./../../config"));

const debug = require("debug")("space-invaders-online-ws:src:index");
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const { WebSocket, WebSocketServer } = require("ws");

const players = {};

class Server extends WebSocketServer{
  constructor({ port, perMessageDeflate=false }) {
    super({ port, perMessageDeflate });
  }

  
}

const wss = new Server({ port: 3002, perMessageDeflate: true });

module.exports = Server;
