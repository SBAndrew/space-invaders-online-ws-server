const path = require("path");
require("dotenv").config();
const config = require(path.resolve(__dirname, "./../../../config"));

const logger = require(path.resolve(__dirname, "./../../utils/logger"));
const { Model, DataTypes } = require("sequelize");
const createConnector = require(path.resolve(__dirname, "./../../connectors/mysql/mysql"));

const UsersAccessLevelEnum = require(path.resolve(__dirname, "./../../utils/users_access_level"))

function createUserModel(sequelize) {
  class User extends Model {}
  User.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      username: {
        type: DataTypes.STRING(64),
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING(128),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(128),
        allowNull: false,
      },
      access_level: {
        type: UsersAccessLevelEnum,
        allowNull: true,
        // defaultValue: UsersAccessLevelEnum.UNCONFIRMED_USER, // mysql already has default value
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: true,
        field: "created_at",
        // defaultValue: DataTypes.NOW, // mysql already has default value
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: true,
        field: "updated_at",
        // defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize, // connection instance
      modelName: "User", // choose the model name
      tableName: "users", // the entity from DB
      timestamps: false, // doesn't update the created_at/updated_at fields
    }
  );

  return User;
}

module.exports = createUserModel(
  createConnector(config.mysql)
);
