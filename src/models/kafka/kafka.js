const path = require("path");
const { kafka } = require("../../../config");
require("dotenv").config();
const config = require(path.resolve(__dirname, "./../../../config"));

const logger = require(path.resolve(__dirname, "./../../utils/logger"));
const createKafkaClient = require(path.resolve(
  __dirname,
  "./../../connectors/kafka/kafka"
));

const kafkaClient = createKafkaClient({
  clientId: "kafkaClient",
  brokers: [`${config.kafka.host}:${config.kafka.port}`,],
  username: config.kafka.username,
  password: config.kafka.password
});

const kafkaModel = {};

module.exports = {
  kafkaModel,
}
