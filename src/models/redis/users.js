const path = require("path");
require("dotenv").config();
const config = require(path.resolve(__dirname, "./../../../config"));

const logger = require(path.resolve(__dirname, "./../../utils/logger"));
const createRedisClient = require(path.resolve(
  __dirname,
  "./../../connectors/redis/redis"
));
const createRedlockClient = require(path.resolve(
  __dirname,
  "./../../connectors/redis/redlock"
));

class User {
  constructor() {
    self._redisClient = createRedisClient(config.redis);
    self._redlockClient = createRedlockClient();
  }

  async deleteUserLock() {
    const pattern = `*:${userId}:*`;
    let cursor = "0";
    do {
      const [nextCursor, keys] = await self._redisClient.scan(
        cursor,
        "MATCH",
        pattern
      );
      if (keys.length > 0) {
        await self._redisClient.del(...keys);
      }
      cursor = nextCursor;
    } while (cursor !== "0");
  }
}

module.exports = User;
