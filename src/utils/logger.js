// TO DO LATER: the logs should be collected into an elasticsearch server or smth

const path = require("path");
const debug = require("debug")("space-invaders-online:utils:logger");
const winston = require("winston"); // logger that supports multiple transports (storage device) configured at different log levels
require("winston-daily-rotate-file");

const commonOptions = {
  datePattern: "YYYY-MM-DD-HH",
  zippedArchive: true,
  maxSize: "20m",
  maxFiles: "14d",
}

const errorTransport = new winston.transports.DailyRotateFile({
  ...commonOptions,
  filename: path.resolve(__dirname, "./../../logs/space-invaders-online-server-%DATE%.error.log"),
  level: "error",
});

const infoTransport = new winston.transports.DailyRotateFile({
  ...commonOptions,
  filename: path.resolve(__dirname, "./../../logs/space-invaders-online-server-%DATE%.info.log"),
  level: "info",
});

const onRotate = (oldFilename, newFilename) => {
  debug(`File [${oldFilename}] has reached full capacity. [${newFilename}] will continue stacking the logs.`);
}

errorTransport.on("rotate", onRotate);
infoTransport.on("rotate", onRotate);

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "space-invaders-online-server" },
  transports: [
    errorTransport,
    infoTransport,
  ],
});

if (process.env.NODE_ENV !== "production") {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.simple(),
    ),
  }));
}

module.exports = logger;
