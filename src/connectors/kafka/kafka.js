const path = require("path");

const logger = require(path.resolve(__dirname, "./../../utils/logger"));
const { Kafka } = require("kafkajs");

function createClient({ clientId, brokers, username, password }) {
  return new Kafka({
    clientId: clientId,
    brokers: brokers,
    ssl: false,
    sasl: {
      mechanism: "plain",
      username: username,
      password: password,
    },
  });
}

module.exports = createClient;
