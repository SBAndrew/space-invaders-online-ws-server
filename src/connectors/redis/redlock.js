const path = require("path");
require("dotenv").config();
const config = require(path.resolve(__dirname, "./../../../config"));
const logger = require(path.resolve(__dirname, "./../../utils/logger"));

const Client = require("ioredis");
const { default: Redlock } = require("redlock");

// You should have one client for each independent redis node or cluster
const redisA = new Client(config.redis);

function createRedlockClient() {
  return new Redlock([redisA], {
    driftFactor: 0.01,
    retryCount: 10,
    retryDelay: 200,
    retryJitter: 200,
    automaticExtensionThreshold: 500,
  });
}

module.exports = createRedlockClient;
