const path = require("path");

const logger = require(path.resolve(__dirname, "./../../utils/logger"));
const Redis = require("ioredis");

function createClient({ host, port, username, password, db }) {
  return new Redis({
    port: port,
    host: host,
    username: username,
    password: password,
    db: db,
  });
}

module.exports = createClient;
