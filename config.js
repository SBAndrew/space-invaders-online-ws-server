const config = {
  mysql: {
    host: process.env.MYSQL_DB_HOST || "",
    port: process.env.MYSQL_DB_PORT || "",
    username: process.env.MYSQL_DB_USERNAME || "",
    password: process.env.MYSQL_DB_PASSWORD || "",
    database: process.env.MYSQL_DB_DATABASE || "",
  },

  redis: {
    host: process.env.REDIS_DB_HOST || `localhost`,
    port: process.env.REDIS_DB_PORT || `6379`,
    username: process.env.REDIS_DB_USERNAME || ``,
    password: process.env.REDIS_DB_PASSWORD || ``,
    db: process.env.REDIS_DB_DATABASE || `0`,
  },

  kafka: {
    host: process.env.KAFKA_HOST || `localhost`,
    port: process.env.KAFKA_PORT || `9094`,
    username: process.env.KAFKA_USERNAME || ``,
    password: process.env.KAFKA_PASSWORD || ``,
  },

  app: {
    port: process.env.PORT || `3001`,
    jwtSecret: process.env.JWT_SECRET || `CHANGE-ME-ASAP`,
  },
};

module.exports = config;
